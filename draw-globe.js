var config = {
  width: 400,
  projection: "orthographic",
  geopos: [14.5820,120.9771],
  interactive: true,
  controls: false,
  background: { fill: "#000000", stroke: "#000000", opacity: 1, width: 4 },
  datapath: "https://ofrohn.github.io/data/",
  container: "celestial-map",
  form: true,
  formFields: {
    "location": true,
    "general": false, 
    "stars": false, 
    "dsos": false,  
    "constellations": true,  
    "lines": true,
    "other": true
  },
  // advanced: false,
  stars: { 
    show: true, 
    limit: 6,
    colors: false,  // Show stars in spectral colors, if not use default color
    style: { fill: "#ffffff", opacity: 1 }, // Default style for stars
    designation: false,
    size: 5,
    propername: true,
    propernameType: "name",
    propernameStyle: { fill: "#fff", font: "10px 'Palatino Linotype', Georgia, Times, 'Times Roman', serif", align: "right", baseline: "bottom" },
    propernameLimit: 1.5,
  },
  planets: {
    show: true,
    which: ["sol", "mer", "ven", "ter", "lun", "mar", "jup", "sat", "ura", "nep"],
    symbols: {  
      "sol": {symbol: "\u2609", letter:"Su", fill: "#ffffff"},
      "mer": {symbol: "\u263f", letter:"Me", fill: "#ffffff"},
      "ven": {symbol: "\u2640", letter:"V", fill: "#ffffff"},
      "ter": {symbol: "\u2295", letter:"T", fill: "#ffffff"},
      "lun": {symbol: "\u25cf", letter:"L", fill: "#ffffff"},
      "mar": {symbol: "\u2642", letter:"Ma", fill: "#ffffff"},
      "cer": {symbol: "\u26b3", letter:"C", fill: "#ffffff"},
      "ves": {symbol: "\u26b6", letter:"Ma", fill: "#ffffff"},
      "jup": {symbol: "\u2643", letter:"J", fill: "#ffffff"},
      "sat": {symbol: "\u2644", letter:"Sa", fill: "#ffffff"},
      "ura": {symbol: "\u2645", letter:"U", fill: "#ffffff"},
      "nep": {symbol: "\u2646", letter:"N", fill: "#ffffff"},
      "plu": {symbol: "\u2647", letter:"P", fill: "#ffffff"},
      "eri": {symbol: "\u26aa", letter:"E", fill: "#ffffff"}
    },
    symbolType: 'disk',
    names: true,
    nameStyle: { fill: "#00ccff", font: "10px 'Lucida Sans Unicode', Consolas, sans-serif", align: "right", baseline: "top" },
  },
  dsos: { show: false },
  mw: { style: { fill: "#ffffff", opacity: "0.05" } },
  constellations: { 
    names: true,
    namesType: 'name',
    nameStyle: { fill: "#fff", align: "center", baseline: "middle", opacity: 1, 
         font: ["14px 'Lucida Sans Unicode', Helvetica, Arial, sans-serif",
                "12px 'Lucida Sans Unicode', Helvetica, Arial, sans-serif",
                "11px 'Lucida Sans Unicode', Helvetica, Arial, sans-serif"]},
    lines: true,
    lineStyle: { stroke: "#ffffff", width: 1.5, opacity: 0.6 },
  },
  lines: { 
    graticule: { show: true, stroke:"#9999cc", width: 1.0, opacity:.3 },
    equatorial: { show: false, stroke:"#aaaaaa", width: 1.5, opacity:.4 },  
    ecliptic: { show: false, stroke:"#66cc66", width: 1.5, opacity:.4 }  
  }
};

// var planets = {sol: "#ff0", lun:"#fff", mer:"#e2e2e2", ven:"#f5f5f0", mar:"#efd1af", jup:"#e6e1df", sat:"#eddebc"};
var planets = {sol: "red", lun:"red", mer:"red", ven:"red", mar:"red", jup:"red", sat:"red"};

// Asterisms canvas style properties for lines and text
var limitMagnitude = 6,
    radius = 1.2,
    grayscale = false,
    starColor = d3.scale.linear()
     .domain([-1.5, 0, limitMagnitude+1])
     .range(['white', 'white', 'black']),
     dt = new Date();  

function isArray(o) { return o !== null && Object.prototype.toString.call(o) === "[object Array]"; }
function arrayfy(o) {
  var res;
  if (!isArray(o)) return [o, o, o];  //It saves some work later, OK?
  if (o.length === 1) return [o[0], o[0], o[0]];
  if (o.length === 2) return [o[0], o[1], o[1]];
  if (o.length >= 3) return o;
}

let bgColor = 'black';
const setMapBgColor = () => {
  if(bgColor === 'black') {
    bgColor = 'white';
    Celestial.apply({
      background: {
        fill: "#ffffff",
        opacity: 1, 
        stroke: "#ffffff",
        width: 4
      },
      stars: { 
        style: { fill: "#000", opacity: 1 },
        propernameStyle: { fill: "#000" },
      },
      planets: {
        symbols: {  
          "sol": {symbol: "\u2609", letter:"Su", fill: "#333"},
          "mer": {symbol: "\u263f", letter:"Me", fill: "#333"},
          "ven": {symbol: "\u2640", letter:"V", fill: "#333"},
          "ter": {symbol: "\u2295", letter:"T", fill: "#333"},
          "lun": {symbol: "\u25cf", letter:"L", fill: "#333"},
          "mar": {symbol: "\u2642", letter:"Ma", fill: "#333"},
          "cer": {symbol: "\u26b3", letter:"C", fill: "#333"},
          "ves": {symbol: "\u26b6", letter:"Ma", fill: "#333"},
          "jup": {symbol: "\u2643", letter:"J", fill: "#333"},
          "sat": {symbol: "\u2644", letter:"Sa", fill: "#333"},
          "ura": {symbol: "\u2645", letter:"U", fill: "#333"},
          "nep": {symbol: "\u2646", letter:"N", fill: "#333"},
          "plu": {symbol: "\u2647", letter:"P", fill: "#333"},
          "eri": {symbol: "\u26aa", letter:"E", fill: "#333"}
        },       
      },
      constellations: { 
        nameStyle: { fill: arrayfy("#000"), align: "center", baseline: "middle", opacity:arrayfy(1), 
             font: ["14px 'Lucida Sans Unicode', Helvetica, Arial, sans-serif",
                    "12px 'Lucida Sans Unicode', Helvetica, Arial, sans-serif",
                    "11px 'Lucida Sans Unicode', Helvetica, Arial, sans-serif"]},
        lineStyle: { stroke: arrayfy("#000000"), width: arrayfy(1.5), opacity: arrayfy(0.6) },
      },
    });
  } else {
    bgColor = 'black';
    Celestial.apply({
      background: {        
        fill: "#000000",  
        opacity: 1, 
        stroke: "#000000", 
        width: 4
      },
      stars: { 
        style: { fill: "#fff", opacity: 1 },
        propernameStyle: { fill: "#fff" },
      },
      planets: {
        symbols: {  
          "sol": {symbol: "\u2609", letter:"Su", fill: "#eee"},
          "mer": {symbol: "\u263f", letter:"Me", fill: "#eee"},
          "ven": {symbol: "\u2640", letter:"V", fill: "#eee"},
          "ter": {symbol: "\u2295", letter:"T", fill: "#eee"},
          "lun": {symbol: "\u25cf", letter:"L", fill: "#eee"},
          "mar": {symbol: "\u2642", letter:"Ma", fill: "#eee"},
          "cer": {symbol: "\u26b3", letter:"C", fill: "#eee"},
          "ves": {symbol: "\u26b6", letter:"Ma", fill: "#eee"},
          "jup": {symbol: "\u2643", letter:"J", fill: "#eee"},
          "sat": {symbol: "\u2644", letter:"Sa", fill: "#eee"},
          "ura": {symbol: "\u2645", letter:"U", fill: "#eee"},
          "nep": {symbol: "\u2646", letter:"N", fill: "#eee"},
          "plu": {symbol: "\u2647", letter:"P", fill: "#eee"},
          "eri": {symbol: "\u26aa", letter:"E", fill: "#eee"}      
        },
      },
      constellations: { 
        nameStyle: { fill: arrayfy("#fff"), align: "center", baseline: "middle", opacity:arrayfy(1), 
             font: ["14px 'Lucida Sans Unicode', Helvetica, Arial, sans-serif",
                    "12px 'Lucida Sans Unicode', Helvetica, Arial, sans-serif",
                    "11px 'Lucida Sans Unicode', Helvetica, Arial, sans-serif"]},
        lineStyle: { stroke: arrayfy("#ffffff"), width: arrayfy(1.5), opacity: arrayfy(0.6) },
      },
    })
  }
}
btnBgColor.addEventListener('click', () => {
  setMapBgColor();
  options.mapColor = bgColor;  
})

Celestial.add({
  type:"json",
  file:"https://ofrohn.github.io/data/stars." + limitMagnitude + ".json",

  callback: function(error, json) {

    // if (error) return console.warn(error);
    // Load the geoJSON file and transform to correct coordinate system, if necessary
    var stars = Celestial.getData(json, config.transform);

    // Add to celestial objects container in d3
    Celestial.container.selectAll(".astars")
       .data(stars.features)
       .enter().append("path")
       .attr("class", "astar");    
    // Trigger redraw to display changes
    Celestial.redraw();
  },

  redraw: function() {
    Celestial.context.globalAlpha = 1;
    // Default color for grayscale
    var indexColor = "#fff", 
        planet;
    // Select the added objects by class name as given previously
    Celestial.container.selectAll(".astar").each(function(d) {
      // If point is visible (this doesn't work automatically for points)
      if (Celestial.clip(d.geometry.coordinates)) {
        // get point coordinates
        var pt = Celestial.mapProjection(d.geometry.coordinates);

        // draw on canvas
        // if not grayscale get the stars color from its b-v index
        if (grayscale === false) indexColor = Celestial.starColor(d); 
        // Set color range to the stars color 
        starColor.range([indexColor, indexColor, 'black']);
        // Set object color
        Celestial.context.fillStyle = starColor(d.properties.mag);
        // Start the drawing path
        Celestial.context.beginPath();
        // Thats a circle in html5 canvas
        Celestial.context.arc(pt[0], pt[1], radius, 0, 2 * Math.PI);
        // Finish the drawing path
        Celestial.context.closePath();
        // Fill the object path with the prevoiusly set fill color
        Celestial.context.fill();
      }
      
    });
    if (!Celestial.origin) return;
    var o = Celestial.origin(dt).spherical();
    for (var key in planets) {
      var planet = Celestial.getPlanet(key, dt);
      if (!Celestial.clip(planet.ephemeris.pos)) continue;
      var pt = Celestial.mapProjection(planet.ephemeris.pos);
      if (key === "lun") {
        Celestial.symbol().type("crescent").size(110).age(planet.ephemeris.age).position(pt)(Celestial.context);
      } else {
        if (key === "sol") radius = 2;
        else radius = 1.2 - (planet.ephemeris.mag + 5) / 10;
        Celestial.context.fillStyle = (bgColor === 'black') ? '#eee' : "#555";
        // Start the drawing path
        Celestial.context.beginPath();
        // draw circle on canvas
        Celestial.context.arc(pt[0], pt[1], radius*2, 0, 2 * Math.PI);
        // Finish the drawing path
        Celestial.context.closePath();
        // Fill the object path with the prevoiusly set fill color
        Celestial.context.fill();
      }
    }
  }
});

Celestial.display(config);
Celestial.date(dt);

document.querySelector('label[title="Local date/time"]').outerHTML = `<br>` + document.querySelector('label[title="Local date/time"]').outerHTML;
