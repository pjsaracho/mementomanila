const mapBorders = [
  {
    name: "Alternating 1_3 Boxes",
    fname: "alternating-1_3-boxes",
    width: 870
  },
  {
    name: "Alternating Map Scale NEW",
    fname: "alternating-map-scale-NEW",
    width: 870
  },
  {
    name: "Alternating Shaded Boxes",
    fname: "alternating-shaded-boxes",
    width: 870
  },
  {
    name: "Black Rough Brush Stroke",
    width: 870
  },
  {
    name: "Loose Circles NEW",
    width: 870
  },
  {
    name: "Map Edge Fade",
    width: 870
  },
  {
    name: "Pointy Scallop",
    width: 870
  },
  {
    name: "Simple Band Medium",
    width: 870
  },
  {
    name: "Simple Band Thin",
    width: 870
  },
  {
    name: "Simple Band Very Thin",
    width: 870
  },
  {
    name: "Simple Band",
    width: 870
  },
  {
    name: "Simple Double Band",
    width: 870
  },
  {
    name: "Thin Map Edge Fade NEW",
    width: 870
  },
  {
    name: "Victorian Fire",
    width: 870
  },
  {
    name: "Vintage Map Border NEW",
    width: 870
  },
  {
    name: "White Rough Brush Stroke",
    width: 870
  },
];

function changeMapBorder(fname, width) {
  document.querySelector('#map-border').style.display = "block";
  document.querySelector('#map-border').src = `../options/map-borders/${fname}.png`;
  options.mapBorder = fname;
}

function removeMapBorder() {
  document.querySelector('#map-border').style.display = "none";
}