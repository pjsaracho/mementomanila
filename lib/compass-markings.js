const compassMarkings = [
  {
    name: "Degree Marks 1 (Thick)",
  },
  {
    name: "Degree Marks 2 (Regular)",
  },
  {
    name: "Degree Marks 3 (Short)",
  },
  {
    name: "Degree Marks 4 (NS)",
  },
  {
    name: "Insatiable North South",
  },
  {
    name: "Large NS",
  },
  {
    name: "Sans North South",
  },
  {
    name: "Small NS",
  },
  {
    name: "Super Compass 1",
  },
  {
    name: "Super Compass 2",
  },
];

function changeCompassMarking(fname) {
  document.querySelector('#map-compass-marking').style.display = "block";
  document.querySelector('#map-compass-marking').src = `../options/compass-markings/${fname}.png`;
  options.compassMarking = fname;
}

function removeMarking() {
  document.querySelector('#map-compass-marking').style.display = "none";
}