const borders = [
  {
    name: "Map Style Edges",
    fname: "map-style-edges",
  },
  {
    name: "Rough Brush Stroke",
    fname: "rough-brush-strokes",
  },
  {
    name: "Thin Solid Margin",
    fname: "thin-solid",
  },
  {
    name: "Thick Solid Margin",
    fname: "thick-solid",
  },
  {
    name: "Border Line 250" ,
    fname: "line-250",
  },
  {
    name: "Border Line 125" ,
    fname: "line-125",
  },
];

function changeBorder(fname,name) {
  document.querySelector('#border').style.display = "block";
  document.querySelector('#border').src = `../options/borders/${fname}.png`;
  options.border = name;
}

function removeBorder() {
  document.querySelector('#border').style.display = "none";
  options.border = "none";
}
